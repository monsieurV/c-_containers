# README #

### What is this repository for? ###

* This repository contain some C++ containers full compatible with the Standard C++ libraries containers.

### How do I get set up? ###

* No particular dependency.

### How do I use it? ###

* Those containers work like the stantard C++ containers.
* The series container is usefull is you need to frequently push data in front or in back of the container. In this case, this is faster than all of the container of the standard library. You can use it like the Vector of the standard template library.
* The underContainer is useful to create a under container in order to browse a container of object/structure like if it was a container of one if the attribute of the object/structure. It may be usefull and faster that a simple browse in some case.