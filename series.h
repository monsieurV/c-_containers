#ifndef STAT_SERIES_H
#define STAT_SERIES_H

#include <iterator>

//TODO : manage error !!!

namespace stat {

template <class value_type>
class Series;

template <class value_type, class base_type>
class series_iterator : public std::iterator<std::random_access_iterator_tag, value_type>
{
private:
    value_type* __pointer;
    Series<base_type>* __base_container;
public:
    series_iterator();
    series_iterator(value_type* pointer, Series<base_type>* base_container);
    value_type& operator *() const;
    value_type* operator ->() const;
    bool operator ==(const series_iterator<value_type, base_type>& __x) const;
    bool operator !=(const series_iterator<value_type, base_type>& __x) const;
    bool operator >(const series_iterator<value_type, base_type>& __x) const;
    bool operator <(const series_iterator<value_type, base_type>& __x) const;
    bool operator >=(const series_iterator<value_type, base_type>& __x) const;
    bool operator <=(const series_iterator<value_type, base_type>& __x) const;
    series_iterator<value_type, base_type> operator + (const std::ptrdiff_t __n) const;
    series_iterator<value_type, base_type>& operator ++ ();
    series_iterator<value_type, base_type> operator ++ (int);
    series_iterator<value_type, base_type>& operator += (const std::ptrdiff_t __n);
    series_iterator<value_type, base_type> operator - (const std::ptrdiff_t __n) const;
    series_iterator<value_type, base_type>& operator -- ();
    series_iterator<value_type, base_type> operator -- (int);
    series_iterator<value_type, base_type>& operator -= (const std::ptrdiff_t __n);
    std::ptrdiff_t operator - (const series_iterator<value_type, base_type>& __n) const;
};

template <class value_type, class base_type>
series_iterator<value_type, base_type>::series_iterator()
    : __pointer(NULL), __base_container(NULL)
{

}

template <class value_type, class base_type>
series_iterator<value_type, base_type>::series_iterator(value_type* pointer, Series<base_type>* base_container)
    : __pointer(pointer), __base_container(base_container)
{

}

template <class value_type, class base_type>
value_type& series_iterator<value_type, base_type>::operator *() const
{
    return *__pointer;
}

template <class value_type, class base_type>
value_type* series_iterator<value_type, base_type>::operator ->() const
{
    return __pointer;
}

template <class value_type, class base_type>
bool series_iterator<value_type, base_type>::operator ==(const series_iterator<value_type, base_type>& __x) const
{
    return __pointer == __x.__pointer;
}

template <class value_type, class base_type>
bool series_iterator<value_type, base_type>::operator !=(const series_iterator<value_type, base_type>& __x) const
{
    return __pointer != __x.__pointer;
}

template <class value_type, class base_type>
bool series_iterator<value_type, base_type>::operator >(const series_iterator<value_type, base_type>& __x) const
{
    return (*this - __x) > 0;
}

template <class value_type, class base_type>
bool series_iterator<value_type, base_type>::operator <(const series_iterator<value_type, base_type>& __x) const
{
    return (*this - __x) < 0;
}

template <class value_type, class base_type>
bool series_iterator<value_type, base_type>::operator >=(const series_iterator<value_type, base_type>& __x) const
{
    return (*this - __x) >= 0;
}

template <class value_type, class base_type>
bool series_iterator<value_type, base_type>::operator <=(const series_iterator<value_type, base_type>& __x) const
{
    return (*this - __x) <= 0;
}

template <class value_type, class base_type>
series_iterator<value_type, base_type> series_iterator<value_type, base_type>::operator + (const std::ptrdiff_t __n) const
{
    series_iterator new_iterator(*this);
    new_iterator += __n;

    return new_iterator;
}

template <class value_type, class base_type>
series_iterator<value_type, base_type>& series_iterator<value_type, base_type>::operator ++ ()
{
    ++__pointer;
    __pointer = (__pointer >= __base_container->__memory_end) ? __base_container->__memory_begin : __pointer;

    return *this;
}

template <class value_type, class base_type>
series_iterator<value_type, base_type> series_iterator<value_type, base_type>::operator ++ (int)
{
    series_iterator<value_type, base_type> temp = *this;
    ++__pointer;
    __pointer = (__pointer >= __base_container->__memory_end) ? __base_container->__memory_begin : __pointer;

    return temp;
}

template <class value_type, class base_type>
series_iterator<value_type, base_type>& series_iterator<value_type, base_type>::operator += (const std::ptrdiff_t __n)
{
    value_type*  temp = __pointer + __n;
    temp     = (temp >= __base_container->__memory_end) ? __base_container->__memory_begin : temp;

    if (temp > __base_container->__end) {
        //tmp throw error !!
    }

    __pointer = temp;

    return *this;
}

template <class value_type, class base_type>
series_iterator<value_type, base_type> series_iterator<value_type, base_type>::operator - (const std::ptrdiff_t __n) const
{
    series_iterator new_iterator(*this);
    new_iterator -= __n;

    return new_iterator;
}

template <class value_type, class base_type>
std::ptrdiff_t series_iterator<value_type, base_type>::operator - (const series_iterator<value_type, base_type>& __n) const
{// this fonction assume that iterators are between __begin and __last included !
    if (__base_container->__begin > __base_container->__end) {
        if (__n.__pointer >= __base_container->__begin && __pointer <= __base_container->__end) {
            return __base_container->__memory_size - (__n.__pointer - __pointer);
        } else if (__n.__pointer <= __base_container->__end && __pointer >= __base_container->__end) {
            return - __base_container->__memory_size - (__n.__pointer - __pointer);
        }
    }

    return __pointer - __n.__pointer;
}

template <class value_type, class base_type>
series_iterator<value_type, base_type>& series_iterator<value_type, base_type>::operator -- ()
{
    --__pointer;
    __pointer = (__pointer < __base_container->__memory_begin) ? (__base_container->__memory_end - 1) : __pointer;

    return *this;
}

template <class value_type, class base_type>
series_iterator<value_type, base_type> series_iterator<value_type, base_type>::operator -- (int)
{
    series_iterator<value_type, base_type> temp = *this;
    --__pointer;
    __pointer = (__pointer < __base_container->__memory_begin) ? (__base_container->__memory_end - 1) : __pointer;

    return temp;
}

template <class value_type, class base_type>
series_iterator<value_type, base_type>& series_iterator<value_type, base_type>::operator -= (const std::ptrdiff_t __n)
{
    value_type* temp = __pointer - __n;
    temp = (temp < __base_container->__memory_begin) ? (__base_container->__memory_end - 1) : temp;

    if (temp < __base_container->__begin) {
        //tmp throw error !!
    }

    __pointer = temp;

    return *this;
}

template <class T>
class Series
{
public:
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;
    typedef T value_type;
    typedef value_type& reference;
    typedef value_type const & const_reference;
    typedef series_iterator<value_type, value_type> iterator;
    typedef series_iterator<value_type const, value_type> const_iterator;
    typedef value_type* pointer;
    typedef value_type const * const_pointer;

    template <class value_type, class base_type>
    friend class series_iterator;

private:
    size_type __size;
    size_type __capacity;
    size_type __memory_size;
    value_type* __memory_begin;
    value_type* __memory_end;
    value_type* __begin;
    value_type* __end;

public:
    Series();
    Series(const Series<value_type>& __copy);
    ~Series();

    Series<value_type>& operator =(const Series<value_type>& __copy);

    void resize(size_type __new_size);
    void reserve(size_type __new_capacity);

    bool empty() const;
    size_type size() const;
    size_type capacity() const;

    void push_back(const value_type __new_data);
    void push_front(const value_type __new_data);
    void pop_back();
    void pop_front();

    iterator begin();
    iterator end();
    const_iterator cbegin();
    const_iterator cend();
    value_type& front() const;
    value_type& back() const;
    value_type& at(size_type __n) const;
    value_type& operator [] (size_type __n) const;
    void clear();
    void assign(int __n, const value_type &__value);
    template <class InputIterator>
    void assign(InputIterator __first, InputIterator __last);
    void swap(Series<value_type>& __x);

private:
    void _copy_data(const Series<value_type>& __copy);
    void _size_check();
    void _clone(const Series<value_type>& __copy);
};

template <class value_type>
Series<value_type>::Series()
 :  __size(0), __capacity(0), __memory_size(0), __memory_begin(NULL), __memory_end(NULL), __begin(NULL), __end(NULL)
{

}

template <class value_type>
Series<value_type>::Series(const Series<value_type> &__copy)
    : __size(0), __capacity(0), __memory_size(0), __memory_begin(NULL), __memory_end(NULL), __begin(NULL), __end(NULL)
{
    _copy_data(__copy);
}

template <class value_type>
Series<value_type>::~Series()
{
    if (__memory_begin != NULL) {
        delete[] __memory_begin;
    }
}

template <class value_type>
Series<value_type>& Series<value_type>::operator =(const Series<value_type>& __copy)
{
    clear();
    _copy_data(__copy);

    return *this;
}

template <class value_type>
void Series<value_type>::_copy_data(const Series<value_type>& __copy)
{
    reserve(__copy.__size);

    for (int i = 0; i < __copy.__size; ++i) {
        __memory_begin[i] = __copy[i];
    }

    __size  = __copy.__size;
    __begin = __memory_begin;
    __end   = __begin + __size;
}

template <class value_type>
void Series<value_type>::resize(size_type __new_size)
{
    reserve(__new_size);

    if (__new_size < __size) {
        int dif = __size - __new_size;
        __end -= dif;
        __end = (__end < __memory_begin) ? (__end + __memory_size) : __end;
        __size = __new_size;

    } else if (__new_size > __size) { //tmp code !!
        int dif = __new_size - __size;
        value_type* new_end = __end + dif;
        new_end = (new_end >= __memory_end) ? (new_end - __memory_size) : new_end;

        for (value_type* pt = __end; pt != new_end; ++pt) {
            pt = (pt >= __memory_end) ? (pt - __memory_size) : pt;

            *pt = 0;
        }

        __end = new_end;
        __size = __new_size;
    }
}

template <class value_type>
void Series<value_type>::reserve(size_type __new_capacity)
{ // tmp trow error if there is not enought memory
    if (__new_capacity > __capacity) {
        if (this->capacity() == 0) {
            __capacity = __new_capacity;
            __memory_size = __capacity + 1;
            __begin = __end = __memory_begin = new value_type[__memory_size];
            __memory_end = __memory_begin + __memory_size;
        } else {
            // tmp : don't reallocate in an other memory space if possible !
            value_type* memory_begin_temp = new value_type[__new_capacity + 1];

            for (int i = 0; i < __size; ++i) {
                memory_begin_temp[i] = at(i);
            }

            delete[] __memory_begin;
            __capacity = __new_capacity;
            __memory_size = __capacity + 1;
            __begin = __memory_begin = memory_begin_temp;
            __end = __memory_begin + __size;
            __memory_end = __memory_begin + __memory_size;
        }
    }
}

template <class value_type>
void Series<value_type>::_size_check()
{
    if (__size >= __capacity) {
        int new_capacity = (__capacity > 0) ? (2 * __capacity) : 1;
        reserve(new_capacity);
    }
}

template <class value_type>
void Series<value_type>::push_back(const value_type __new_data)
{
    _size_check();
    *__end = __new_data;
    ++__end;
    __end = (__end == __memory_end) ? __memory_begin : __end;
    ++__size;
}

template <class value_type>
void Series<value_type>::push_front(const value_type __new_data)
{
    _size_check();
    --__begin;
    __begin = (__begin < __memory_begin) ? (__memory_end - 1) : __begin;
    *__begin = __new_data;
    ++__size;
}

template <class value_type>
void Series<value_type>::pop_back()
{
    if (__size > 0) {
        --__end;
        __end = (__end < __memory_begin) ? (__memory_end - 1) : __end;
        --__size;
    }
}

template <class value_type>
void Series<value_type>::pop_front()
{
    if (__size > 0) {
        ++__begin;
        __begin = (__begin == __memory_end) ? __memory_begin : __begin;
        --__size;
    }
}

template <class value_type>
typename Series<value_type>::iterator Series<value_type>::begin()
{
    return iterator(__begin, this);
}

template <class value_type>
typename Series<value_type>::iterator Series<value_type>::end()
{
    return iterator(__end, this);
}

template <class value_type>
typename Series<value_type>::const_iterator Series<value_type>::cbegin()
{
    return const_iterator(__begin, this);
}

template <class value_type>
typename Series<value_type>::const_iterator Series<value_type>::cend()
{
    return const_iterator(__end, this);
}

template <class value_type>
value_type& Series<value_type>::front() const
{
    return at(0);
}

template <class value_type>
value_type& Series<value_type>::back() const
{
    return at(__size - 1);
}

template <class value_type>
value_type& Series<value_type>::at(size_type __n) const
{
    //_range_check(__n); // tmp

    value_type* data = __begin + __n;
    data    = (data >= __memory_end) ? (data - __memory_size) : data;

    return *data;
}

template <class value_type>
value_type& Series<value_type>::operator [] (size_type __n) const
{
    return at(__n);
}

template <class value_type>
void Series<value_type>::clear()
{
    __size  = 0;
    __begin = __end = __memory_begin;
}

template <class value_type>
bool Series<value_type>::empty() const
{
    return (__size == 0);
}

template <class value_type>
typename Series<value_type>::size_type Series<value_type>::size() const
{
    return __size;
}

template <class value_type>
typename Series<value_type>::size_type Series<value_type>::capacity() const
{
    return __capacity;
}

template <class value_type>
void Series<value_type>::_clone(const Series<value_type> &__copy)
{
    __size         = __copy.__size;
    __capacity     = __copy.__capacity;
    __memory_size  = __copy.__memory_size;
    __memory_begin = __copy.__memory_begin;
    __memory_end   = __copy.__memory_end;
    __begin        = __copy.__begin;
    __end          = __copy.__end;
}

template <class value_type>
void Series<value_type>::swap(Series<value_type>& __x)
{
    Series<value_type> temp;
    temp._clone(__x);
    __x._clone(*this);
    _clone(temp);
    temp.__memory_begin = NULL;
}

template <class value_type>
void Series<value_type>::assign(int __n, const value_type& __value)
{
    //tmp duplicated code
    reserve(__n);
    __size  = __n;
    __begin = __memory_begin;
    __end   = __memory_begin + __size;

    for (int i = 0; i < __size; ++i) {
        __begin[i] = __value;
    }
}

template <class value_type>
template <class InputIterator>
void Series<value_type>::assign(InputIterator __first, InputIterator __last)
{
    int n = __last - __first;
    //tmp duplicated code
    reserve(n);
    __size = n;
    __begin = __memory_begin;
    __end   = __memory_begin + __size;

    int i = 0;
    for (InputIterator it = __first; it != __last; ++it) {
        __begin[i] = *it;
        ++i;
    }
}

} // namespace stat

#endif // STAT_SERIES_H
