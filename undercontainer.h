#ifndef UNDERSERIES_H
#define UNDERSERIES_H

#include <iterator>

#include <iostream>

namespace stat {

template <class container_type, class T>
class UnderContainer;

template <class value_type, class base_iterator>
class undercontaineur_iterator : public std::iterator<std::random_access_iterator_tag, value_type> // tmp comparaison with 2 underclass with diferent attr(and same type) possible...
{
private:
    std::size_t __memory_dif; // tmp size_type ??
    value_type* __pointer;
    base_iterator __base_it;
public:
    undercontaineur_iterator();
    undercontaineur_iterator(std::size_t memory_dif);
    undercontaineur_iterator(std::size_t memory_dif, base_iterator base_it);
    value_type& operator *() const;
    value_type* operator ->() const;
    bool operator ==(const undercontaineur_iterator<value_type, base_iterator>& __x) const;
    bool operator !=(const undercontaineur_iterator<value_type, base_iterator>& __x) const;
    bool operator >(const undercontaineur_iterator<value_type, base_iterator>& __x) const;
    bool operator <(const undercontaineur_iterator<value_type, base_iterator>& __x) const;
    bool operator >=(const undercontaineur_iterator<value_type, base_iterator>& __x) const;
    bool operator <=(const undercontaineur_iterator<value_type, base_iterator>& __x) const;
    undercontaineur_iterator<value_type, base_iterator> operator + (const std::ptrdiff_t __n) const;
    undercontaineur_iterator<value_type, base_iterator>& operator ++ ();
    undercontaineur_iterator<value_type, base_iterator> operator ++ (int);
    undercontaineur_iterator<value_type, base_iterator>& operator += (const std::ptrdiff_t __n);
    undercontaineur_iterator<value_type, base_iterator> operator - (const std::ptrdiff_t __n) const;
    undercontaineur_iterator<value_type, base_iterator>& operator -- ();
    undercontaineur_iterator<value_type, base_iterator> operator -- (int);
    undercontaineur_iterator<value_type, base_iterator>& operator -= (const std::ptrdiff_t __n);
    std::ptrdiff_t operator - (const undercontaineur_iterator<value_type, base_iterator>& __n) const;

    //get_origin_it
    //get_origin_value
private:
    void _update_pointer();
};

template <class value_type, class base_iterator>
undercontaineur_iterator<value_type, base_iterator>::undercontaineur_iterator()
    :__memory_dif(0)
{

}

template <class value_type, class base_iterator>
undercontaineur_iterator<value_type, base_iterator>::undercontaineur_iterator(std::size_t memory_dif)
    :__memory_dif(memory_dif)
{

}

template <class value_type, class base_iterator>
undercontaineur_iterator<value_type, base_iterator>::undercontaineur_iterator(std::size_t memory_dif, base_iterator base_it)
    :__memory_dif(memory_dif), __base_it(base_it)
{
    _update_pointer();
}

template <class value_type, class base_iterator>
value_type& undercontaineur_iterator<value_type, base_iterator>::operator *() const
{
    return *__pointer;
}

template <class value_type, class base_iterator>
value_type* undercontaineur_iterator<value_type, base_iterator>::operator ->() const
{
    return __pointer;
}

template <class value_type, class base_iterator>
bool undercontaineur_iterator<value_type, base_iterator>::operator ==(const undercontaineur_iterator<value_type, base_iterator>& __x) const
{
    return __base_it == __x.__base_it;
}

template <class value_type, class base_iterator>
bool undercontaineur_iterator<value_type, base_iterator>::operator !=(const undercontaineur_iterator<value_type, base_iterator>& __x) const
{
    return __base_it != __x.__base_it;
}

template <class value_type, class base_iterator>
bool undercontaineur_iterator<value_type, base_iterator>::operator >(const undercontaineur_iterator<value_type, base_iterator>& __x) const
{
    return __base_it > __x.__base_it;
}

template <class value_type, class base_iterator>
bool undercontaineur_iterator<value_type, base_iterator>::operator <(const undercontaineur_iterator<value_type, base_iterator>& __x) const
{
    return __base_it < __x.__base_it;
}

template <class value_type, class base_iterator>
bool undercontaineur_iterator<value_type, base_iterator>::operator >=(const undercontaineur_iterator<value_type, base_iterator>& __x) const
{
    return __base_it >= __x.__base_it;
}

template <class value_type, class base_iterator>
bool undercontaineur_iterator<value_type, base_iterator>::operator <=(const undercontaineur_iterator<value_type, base_iterator>& __x) const
{
    return __base_it <= __x.__base_it;
}

template <class value_type, class base_iterator>
undercontaineur_iterator<value_type, base_iterator> undercontaineur_iterator<value_type, base_iterator>::operator + (const std::ptrdiff_t __n) const
{
    undercontaineur_iterator<value_type, base_iterator> new_iterator(*this);
    new_iterator += __n;

    return new_iterator;
}

template <class value_type, class base_iterator>
undercontaineur_iterator<value_type, base_iterator>& undercontaineur_iterator<value_type, base_iterator>::operator ++ ()
{
    ++__base_it;
    _update_pointer();

    return *this;
}

template <class value_type, class base_iterator>
undercontaineur_iterator<value_type, base_iterator> undercontaineur_iterator<value_type, base_iterator>::operator ++ (int)
{
    undercontaineur_iterator<value_type, base_iterator> temp = *this;
    ++__base_it;
    _update_pointer();

    return temp;
}

template <class value_type, class base_iterator>
undercontaineur_iterator<value_type, base_iterator>& undercontaineur_iterator<value_type, base_iterator>::operator += (const std::ptrdiff_t __n)
{
    __base_it += __n;
    _update_pointer();

    return *this;
}

template <class value_type, class base_iterator>
undercontaineur_iterator<value_type, base_iterator> undercontaineur_iterator<value_type, base_iterator>::operator - (const std::ptrdiff_t __n) const
{
    undercontaineur_iterator<value_type, base_iterator> new_iterator(*this);
    new_iterator -= __n;

    return new_iterator;
}

template <class value_type, class base_iterator>
undercontaineur_iterator<value_type, base_iterator>& undercontaineur_iterator<value_type, base_iterator>::operator -- ()
{
    --__base_it;
    _update_pointer();

    return *this;
}

template <class value_type, class base_iterator>
undercontaineur_iterator<value_type, base_iterator> undercontaineur_iterator<value_type, base_iterator>::operator -- (int)
{
    undercontaineur_iterator<value_type, base_iterator> temp = *this;
    +--__base_it;
    _update_pointer();

    return temp;
}

template <class value_type, class base_iterator>
undercontaineur_iterator<value_type, base_iterator>& undercontaineur_iterator<value_type, base_iterator>::operator -= (const std::ptrdiff_t __n)
{
    __base_it -= __n;
    _update_pointer();

    return *this;
}

template <class value_type, class base_iterator>
std::ptrdiff_t undercontaineur_iterator<value_type, base_iterator>::operator - (const undercontaineur_iterator<value_type, base_iterator>& __n) const
{
    return __base_it - __n.__base_it;
}

template <class value_type, class base_iterator>
void undercontaineur_iterator<value_type, base_iterator>::_update_pointer()
{
    __pointer = (value_type*) ((std::size_t) &*__base_it + __memory_dif);
}

template <class container_type, class T>
class UnderContainer
{
public:
    typedef std::size_t size_type;
    typedef std::ptrdiff_t difference_type;
    typedef T value_type;
    typedef value_type& reference;
    typedef value_type const & const_reference;
    typedef undercontaineur_iterator<value_type, typename container_type::iterator> iterator;
    typedef undercontaineur_iterator<value_type const, typename container_type::iterator> const_iterator;
    typedef value_type* pointer;
    typedef value_type const * const_pointer;

private:
    container_type* __base_container;
    size_type __memory_dif;
public:
    UnderContainer();
    UnderContainer(container_type* base_container, value_type* begin);
    UnderContainer(container_type* base_container, typename container_type::pointer base, value_type* value);
    UnderContainer(container_type* base_container, size_type memory_dif);

    bool empty() const;
    size_type size() const;
    iterator begin();
    iterator end();
    iterator get_it_at(container_type::iterator base_it);
    const_iterator cbegin();
    const_iterator cend();
    const_iterator get_cit_at(container_type::iterator base_it);
    value_type& front() const;
    value_type& back() const;
    value_type& at(size_type __n) const;
    value_type& operator [] (size_type __n) const;

    container_type& get_base_container();
    typename container_type::value_type& get_base_value_at(size_type __n);
    typename container_type::iterator get_base_iterator_at(size_type __n);

private:
    void _set_memory_dif(size_type memory_dif);
};

template <class container_type, class value_type>
UnderContainer<container_type, value_type>::UnderContainer()
{
    //never use a UnderContainer default_constructed
}

template <class container_type, class value_type>
UnderContainer<container_type, value_type>::UnderContainer(container_type* base_container, value_type* begin)
    : __base_container(base_container)
{
    if (__base_container->size() == 0) {
        //tmp trow error
    }

    _set_memory_dif(((size_type) begin) - ((size_type) &*__base_container->begin()));
}

template <class container_type, class value_type>
UnderContainer<container_type, value_type>::UnderContainer(container_type* base_container, typename container_type::pointer base, value_type* value)
    : __base_container(base_container)
{
    _set_memory_dif(((size_type) value) - ((size_type) base));
}

template <class container_type, class value_type>
UnderContainer<container_type, value_type>::UnderContainer(container_type* base_container, size_type memory_dif)
    : __base_container(base_container)
{
    _set_memory_dif(memory_dif);
}

template <class container_type, class value_type>
void UnderContainer<container_type, value_type>::_set_memory_dif(size_type memory_dif)
{
    if (memory_dif > sizeof(typename container_type::value_type)) {
        //tmp trow error
    }

    __memory_dif = memory_dif;
}

template <class container_type, class value_type>
bool UnderContainer<container_type, value_type>::empty() const
{
    return __base_container->empty();
}

template <class container_type, class value_type>
typename UnderContainer<container_type, value_type>::size_type UnderContainer<container_type, value_type>::size() const
{
    return __base_container->size();
}

template <class container_type, class value_type>
typename UnderContainer<container_type, value_type>::iterator UnderContainer<container_type, value_type>::begin()
{
    return iterator(__memory_dif, __base_container->begin());
}

template <class container_type, class value_type>
typename UnderContainer<container_type, value_type>::iterator UnderContainer<container_type, value_type>::end()
{
    return iterator(__memory_dif, __base_container->end());
}

template <class container_type, class value_type>
typename UnderContainer<container_type, value_type>::iterator UnderContainer<container_type, value_type>::get_it_at(container_type::iterator base_it)
{
    return iterator(__memory_dif, base_it);
}

template <class container_type, class value_type>
typename UnderContainer<container_type, value_type>::const_iterator UnderContainer<container_type, value_type>::cbegin()
{
    return const_iterator(__memory_dif, __base_container->begin());
}

template <class container_type, class value_type>
typename UnderContainer<container_type, value_type>::const_iterator UnderContainer<container_type, value_type>::cend()
{
    return const_iterator(__memory_dif, __base_container->end());
}

template <class container_type, class value_type>
typename UnderContainer<container_type, value_type>::iterator UnderContainer<container_type, value_type>::get_cit_at(container_type::iterator base_it)
{
    return const_iterator(__memory_dif, base_it);
}

template <class container_type, class value_type>
value_type& UnderContainer<container_type, value_type>::front() const
{
    return *((value_type*) ((size_type) &__base_container->front() + __memory_dif));
}

template <class container_type, class value_type>
value_type& UnderContainer<container_type, value_type>::back() const
{
    return *((value_type*) ((size_type) &__base_container->back() + __memory_dif));
}

template <class container_type, class value_type>
value_type& UnderContainer<container_type, value_type>::at(size_type __n) const
{
    return *((value_type*) ((size_type) &__base_container->at(__n) + __memory_dif));
}

template <class container_type, class value_type>
value_type& UnderContainer<container_type, value_type>::operator [] (size_type __n) const
{
    return at(__n);
}

template <class container_type, class value_type>
container_type& UnderContainer<container_type, value_type>::get_base_container()
{
    return *__base_container;
}

template <class container_type, class value_type>
typename container_type::value_type& UnderContainer<container_type, value_type>::get_base_value_at(size_type __n)
{
    return &__base_container->at(__n);
}

template <class container_type, class value_type> // tmp usefull or enought performant ????
typename container_type::iterator UnderContainer<container_type, value_type>::get_base_iterator_at(size_type __n)
{

}

} //namespace stat

#endif // UNDERSERIES_H
